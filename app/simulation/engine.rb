# frozen_string_literal: true

module Simulation
  class Engine
    def initialize(args)
      @model = Simulation::Model::LoadBalancer.new(args)
    end

    def tick(args)
      @model.tick(args)
    end
  end
end
