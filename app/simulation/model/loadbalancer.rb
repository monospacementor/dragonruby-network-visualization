# frozen_string_literal: true

module Simulation
  module Model
    class LoadBalancer < Simulation::Model::Base
      def initialize(args)
        super
      end

      def initialize_entities
        @cluster = create_cluster
        @loadbalancer = create_loadbalancer
        add_cluster_members
        create_emitter(@loadbalancer)
      end

      def tick(args)
        super

        add_receiver if args.inputs.keyboard.key_down.a
        remove_receiver if args.inputs.keyboard.key_down.s
      end

      private

      # @param [Array] receivers A list of receiving Endpoints
      # @return RoundRobinLoadbalancer The LB entity
      #
      def create_loadbalancer
        lb = Simulation::Network::Node::LoadBalancer::RoundRobin.new(
          self, name: "sender", pos_x: 615, pos_y: 300,
                receivers: [],
        )
        add_entity(lb)
        lb
      end

      # @return [Cluster] Cluster of endpoints
      def create_cluster
        cluster = Simulation::Network::Cluster.new(
          self,
          pos_y: 500,
        )

        add_entity(cluster)
        cluster
      end

      def add_cluster_members
        receiver_count.times do
          add_receiver
        end
      end

      def add_receiver
        receiver = Simulation::Network::Node::Receiver.new(
          self,
          processing_time: processing_time,
        )
        @cluster.add_member(receiver)
        @loadbalancer.receivers = @cluster.member_list
      end

      def remove_receiver
        @cluster.shrink
        @loadbalancer.receivers = @cluster.member_list
      end

      def create_emitter(target)
        emitter = Simulation::Network::Node::RandomRequestEmitter.new(
          self,
          pos_x: 615, pos_y: 100,
          receiver: target,
          spawn_min: sender_spawn_min, spawn_max: sender_spawn_max
        )
        add_entity(emitter)
      end

      def receiver_count
        1
      end

      def processing_time
        10 + rand(50)
      end

      def sender_spawn_min
        10
      end

      def sender_spawn_max
        30
      end
    end
  end
end
