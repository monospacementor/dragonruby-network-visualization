# frozen_string_literal: true

#
# Simulation model
#

module Simulation
  module Model
    class Base
      def initialize(args)
        @args = args

        @entities = {}

        initialize_entities
      end

      def tick(args)
        @entities.each_pair do |_id, entity|
          entity.tick(args)
        end
      end

      def add_entity(entity)
        return if @entities.key?(entity.id)

        @entities[entity.id] = entity
      end

      def remove_entity(entity)
        @entities.delete(entity.id)
      end

      def initialize_entities
        raise "Abstract base class!"
      end

      def intersect(entity1, entity2)
        if entity1.bounding_box.intersect_rect?(entity2.bounding_box)
          entity1.hit(entity2)
          entity2.hit(entity1)

          return true
        end

        false
      end

      private

      attr_reader :args, :entities
    end
  end
end
