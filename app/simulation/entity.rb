# frozen_string_literal: true

module Simulation
  class Entity
    attr_accessor :model, :name, :pos_x, :pos_y, :v_x, :v_y, :ttl
    attr_reader :age

    def initialize(engine, attributes = {})
      @model = engine

      @pos_x = attributes[:pos_x] || 0
      @pos_y = attributes[:pos_y] || 0
      @v_x = attributes[:v_x] || 0
      @v_y = attributes[:v_y] || 0

      @age = 0
      @ttl = attributes[:ttl]
    end

    def id
      object_id
    end

    def tick(_args)
      increase_age
      handle_ttl
      move
    end

    def increase_age
      @age += 1
    end

    def handle_ttl
      return if @ttl.nil?

      @ttl -= 1
      return unless @ttl.zero?

      model.remove_entity(self)
    end

    def move
      @pos_x += @v_x if @v_x
      @ttl = 1 if @pos_x.negative? || @pos_x > 1280

      @pos_y += @v_y if @v_y
      @ttl = 1 if @pos_y.negative? || @pos_y > 720
    end

    def intersect(entity)
      model.intersect(self, entity)
    end

    def hit(entity)
      ;
    end

    def bounding_box
      nil
    end
  end
end
