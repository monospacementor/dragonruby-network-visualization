# frozen_string_literal: true

module Simulation
  module Network

    class Request < Simulation::Entity
      def initialize(engine, attributes = {})
        super

        @sprite = DragonRuby::Sprite.new(
          x: pos_x, y: pos_y, w: 20, h: 20,
          path: "sprites/square/blue.png"
        )

        @receiver = attributes.fetch(:receiver)

        calc_trajectory
        @state = :in_transit
      end

      def tick(args)
        super

        calc_trajectory

        if @state == :dropped
          @v_y *= 1.05
        end

        intersect(@receiver) if @state == :in_transit
        render(args)
      end

      def render(args)
        @sprite.x = pos_x
        @sprite.y = pos_y
        args.outputs.sprites << @sprite.serialize
      end

      def bounding_box
        @sprite.serialize
      end

      def hit(entity)
        if entity == @receiver
          if @receiver.connect
            request_accepted
          else
            request_dropped
          end
        end
      end

      def center_x
        @pos_x + 10
      end

      def center_y
        @pos_y + 10
      end

      private

      def calc_trajectory
        return unless @state == :in_transit

        @v_x = (@receiver.center_x - center_x) / (60 - age)
        @v_y = (@receiver.center_y - center_y) / (60 - age)
      end

      def request_accepted
        @state = :accepted
        @ttl = 5
      end

      def request_dropped
        @state = :dropped
        @sprite.path = "sprites/square/red.png"
        @v_x = 1 + (rand(5) * [1, -1].sample)
        @v_y = -0.5 * @v_y
        @ttl = 30
      end
    end
  end
end
