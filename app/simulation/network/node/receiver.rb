# frozen_string_literal: true

module Simulation
  module Network
    module Node
      class Receiver < Base
        def initialize(engine, attributes = {})
          super

          @processing_time = attributes[:processing_time]
          @busy = false
          @accepted_counter = 0
          @dropped_counter = 0
        end

        def tick(args)
          super

          render(args)
          handle_busy_state(args)
        end

        def connect
          if @busy
            @dropped_counter += 1
            return false
          end

          @accepted_counter += 1
          enter_busy_state
          true
        end

        def render(args)
          super

          args.outputs.labels << DragonRuby::Label.new(
            x: pos_x,
            y: pos_y,
            text: @processing_time,
          )
          args.outputs.labels << DragonRuby::Label.new(
            x: pos_x,
            y: pos_y + 70,
            text: "R: #{connect_ratio}%",
          )
        end

        private

        def enter_busy_state
          @busy = true
          @busy_timer = @processing_time
        end

        def exit_busy_state
          @busy = false
        end

        def handle_busy_state(args)
          if @busy
            render_busy_state(args)

            @busy_timer -= 1
            if @busy_timer.zero?
              exit_busy_state
            end
          end
        end

        def render_busy_state(args)
          sprite = DragonRuby::Sprite.new(
            x: center_x, y: center_y,
            anchor_x: 0.5, anchor_y: 0.5,
            w: busy_indicator_size, h: busy_indicator_size,
            path: "sprites/circle/green.png",
            angle: 90 + (@busy_timer * (360 / @processing_time)),
          )
          args.outputs.sprites << sprite
        end

        def busy_indicator_size
          @busy_timer * ((SIZE - 10) / @processing_time)
        end

        def connect_ratio
          total = @accepted_counter + @dropped_counter
          if total.zero?
            0
          else
            (100 * @accepted_counter / (total)).to_i
          end
        end
      end
    end
  end
end