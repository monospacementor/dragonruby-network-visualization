module Simulation
  module Network
    module Node
      module LoadBalancer
        class Base < Node::Base
          attr_accessor :receivers

          def initialize(engine, attributes = {})
            super

            @receivers = attributes[:receivers]
          end

          def tick(args)
            super

            render(args)
          end

          def connect
            spawn_request

            true
          end

          private

          def spawn_request
            return if @receivers.empty?

            request = Request.new(
              model,
              receiver: pick_receiver,
              pos_x: pos_x + 15,
              pos_y: pos_y + 15,
            )
            model.add_entity(request)
          end

          def pick_receiver
            fail "Abstract base class!"
          end
        end
      end
    end
  end
end