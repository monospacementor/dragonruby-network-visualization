# frozen_string_literal: true

module Simulation
  module Network
    module Node
      module LoadBalancer
        class RoundRobin < LoadBalancer::Base
          def initialize(engine, attributes = {})
            super

            @round_robin = 0
          end

          def render(args)
            super

            args.outputs.labels << DragonRuby::Label.new(
              x: pos_x + 50,
              y: pos_y + 20,
              text: "LB",
            )
          end

          private

          def pick_receiver
            return nil if @receivers.empty?

            @round_robin = (@round_robin + 1) % @receivers.length
            @receivers[@round_robin]
          end
        end
      end
    end
  end
end