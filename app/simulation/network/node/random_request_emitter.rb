# frozen_string_literal: true

module Simulation
  module Network
    module Node

      class RandomRequestEmitter < Base
        def initialize(engine, attributes = {})
          super

          @receiver = attributes.fetch(:receiver)
          @spawn_min = attributes.fetch(:spawn_min)
          @spawn_max = attributes.fetch(:spawn_max)
        end

        def tick(args)
          super

          render(args)
          spawn_request(args)
        end

        private

        def render(args)
          super

          args.outputs.labels << DragonRuby::Label.new(
            x: pos_x,
            y: pos_y,
            text: "#{@spawn_min}/#{@spawn_max}",
          )
        end

        def spawn_request(args)
          return unless spawn_condition(args)

          request = Request.new(
            model,
            name: "request_#{args.state.tick_count}",
            receiver: @receiver,
            pos_x: pos_x + 15,
            pos_y: pos_y + 15,
          )
          model.add_entity(request)
        end

        def spawn_condition(args)
          (args.state.tick_count % (@spawn_min + rand(@spawn_max - @spawn_min).to_i)).zero?
        end
      end
    end
  end
end