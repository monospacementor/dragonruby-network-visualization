# frozen_string_literal: true

module Simulation
  module Network
    module Node
      class Base < Simulation::Entity
        SIZE = 50

        attr_accessor :name, :pos_x, :pos_y

        def tick(args)
          super

          render(args)
        end

        def render(args)
          border = DragonRuby::Border.new(
            x: pos_x, y: pos_y, w: SIZE, h: SIZE,
          )
          args.outputs.borders << border.serialize
        end

        def bounding_box
          border = DragonRuby::Border.new(
            x: pos_x, y: pos_y, w: SIZE, h: SIZE,
          )
          border.serialize
        end

        def center_x
          pos_x + (SIZE / 2)
        end

        def center_y
          pos_y + (SIZE / 2)
        end
      end
    end
  end
end