# frozen_string_literal: true

module Simulation
  module Network
    class Cluster < Simulation::Entity
      def initialize(engine, attributes = {})
        super

        @pos_y = attributes.fetch(:pos_y)

        @members = {}
      end

      def member_list
        @members.values
      end

      def tick(args)
        super

        tick_members(args)
      end

      def add_member(member)
        @members[member.id] = member
        redistribute
      end

      def remove_member(member)
        @members.delete(member.id)
        redistribute
      end

      def shrink
        return if @members.length.zero?

        @members.shift
        redistribute
      end

      private

      def tick_members(args)
        @members.each_pair do |_id, member|
          member.tick(args)
        end
      end

      def redistribute
        space = 800 / member_count
        offset = 240 + ((space - 50) / 2)

        index = 0
        @members.each_pair do |_id, member|
          member.pos_x = offset + (index * space)
          member.pos_y = @pos_y
          index += 1
        end
      end

      def member_count
        @members.length
      end
    end
  end
end
