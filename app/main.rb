# frozen_string_literal: true

require_relative "dragonruby"
require_relative "simulation"

#
# DragonRuby main
#

def tick(args)
  init(args) if args.state.tick_count.zero? || args.inputs.keyboard.key_down.r

  engine = args.state.engine
  engine.tick(args)
end

def init(args)
  args.state.engine = Simulation::Engine.new(args)
end

def reset_with(_count: count)
  # rubocop:disable Style/GlobalVars
  $gtk.reset
  # rubocop:enable Style/GlobalVars
end
