# frozen_string_literal: true

module AttrBorder
  include AttrRect

  attr_accessor :x, :y, :w, :h, :r, :g, :b, :a, :anchor_x, :anchor_y, :blendmode_enum

  def primitive_marker
    :border
  end
end

module DragonRuby
  class Sprite
    include AttrSprite

    def initialize(attributes = {})
      self.x = attributes[:x]
      self.y = attributes[:y]
      self.anchor_x = attributes[:anchor_x]
      self.anchor_y = attributes[:anchor_y]
      self.w = attributes[:w]
      self.h = attributes[:h]
      self.path = attributes[:path]
      self.angle = attributes[:angle]
    end

    def serialize
      { x: x, y: y, w: w, h: h, path: path, angle: angle }
    end

    def tick; end
  end

  class Border
    include AttrBorder

    def initialize(attributes = {})
      self.x = attributes[:x]
      self.y = attributes[:y]
      self.w = attributes[:w]
      self.h = attributes[:h]
    end

    def serialize
      { x: x, y: y, w: w, h: h, r: r, g: g, b: b }
    end
  end

  class Label
    include AttrLabel

    def initialize(attributes = {})
      # :x, :y, :z,
      # :text,
      # :size_enum, :alignment_enum,
      # :vertical_alignment_enum,
      # :r, :g, :b, :a,
      # :font,
      # :blendmode_enum,
      # :anchor_x, :anchor_y, :size_px
      self.x = attributes[:x]
      self.y = attributes[:y]
      self.z = attributes[:z]
      self.text = attributes[:text]
    end

    def serialize
      { x: x, y: y, text: text }
    end
  end
end
